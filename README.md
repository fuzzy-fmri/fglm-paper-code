# Fuzzy GLM for functional MRI

This is the companion code for the paper

"Fuzzy General Linear Modeling for Functional Magnetic Resonance Imaging Analysis", by Alejandro Veloz, Claudio Moraga, Héctor Allende, Alejandro Weinstein, Luis Hernández-Garcı́a, Steren Chabert, Rodrigo Salas, Rodrigo Riveros, and Carlos Bennett.

