function t = glm_fuzzy_lr_fs(Y,u,TR)

T = size(Y,1);

[Hc,Hmin,Hmax] = gen_fHRF2(T, TR);

c = [0 1];
[T,n] = size(Y);
p=2;
bold_c = Hc*u;
bold_l = Hmin*u;
bold_r = Hmax*u;

x = [ones(T,1),bold_c];
xl = [ones(T,1),bold_l];
xr = [ones(T,1),bold_r];

y = Y;
xtx = x'*x + xl'*xl + xr'*xr;
xty = (x+xl+xr)'*y;
beta = xtx\xty;
inv_xtx = inv(xtx);
vCon = zeros(1,n);
xbound0p5 = 0.5*(xl+x);
for i = 1:n
    var_est = ((Y(:,i)-xbound0p5*beta(:,i))'*(Y(:,i)-xbound0p5*beta(:,i)))/T;
    vCon(i) = c * ( var_est * eye(p) ) * inv_xtx * c';
end
t = (beta' * c') ./ sqrt(vCon)';
t(isnan(t)) = 0;
t(isinf(t)) = 0;
