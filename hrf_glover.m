function h = hrf_glover(timeline,p)

if nargin < 2
    p = [5.4 6 10.8 12 0.35];
end
tau1 = p(1); delta1 = p(2);
tau2 = p(3); delta2 = p(4);
c = p(5);

gamma1 = ((timeline/tau1).^delta1).*exp(-(delta1/tau1)*(timeline-tau1));
gamma2 = c*((timeline/tau2).^delta2).*exp(-(delta2/tau2)*(timeline-tau2));
h = gamma1 - gamma2;
h = h/max(h);