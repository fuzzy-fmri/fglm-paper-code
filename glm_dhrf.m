function t = glm_dhrf(Y,u,TR)

c = [0 1 0];
[T,N] = size(Y);
timeline = (0:TR:TR*T-TR)';
h = hrf_glover(timeline);
H = Hmat(h); % hrf convolution matrix

dhrf = diff(H(:,1));
dhrf = [0;dhrf];
dH = toeplitz(dhrf, [dhrf(1),zeros(1,T-1)]);
X = ones(T,1);
X = [X,H*u,dH*u];

p = size(X,2);
xtx_inv = pinv(X);
df = T-p+1;
vCon = zeros(1,N);
beta_est = xtx_inv*Y;
xtx_inv = inv(X'*X);
for i = 1:N
    var_est = ((Y(:,i)-X*beta_est(:,i))'*(Y(:,i)-X*beta_est(:,i)))/T;
    vCon(i) = c * var_est * eye(p) * xtx_inv * c';
end
t = (beta_est' * c') ./ sqrt(vCon)';
t(isnan(t)) = 0;
t(isinf(t)) = 0;