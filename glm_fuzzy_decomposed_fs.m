function t = glm_fuzzy_decomposed_fs(Ydataset,u,TR)

T = size(Ydataset,1);

parameter_index = 1;
v = linspace(-1,5,100); % for the fuzzy HRF
% parameter_index = 3;
% v = linspace(-3,3,100);
%
[Hc,Hmin,Hmax] = gen_fHRF(v, T, TR, parameter_index);

% [Hc,Hmin,Hmax] = gen_fHRF2(T, TR);


c = [0 1 0];
[T,n] = size(Ydataset);
Xfuzzy = [Hmin*u,Hc*u,Hmax*u];

dhrf_c = diff(Hc(:,1));
dhrf_c = [0;dhrf_c];
dH_c = toeplitz(dhrf_c, [dhrf_c(1),zeros(1,T-1)]);
dH_c = dH_c*u;

X = cell(T,3);
for i = 1 : T
    X{i,1} = 1;
    X{i,2} = Xfuzzy(i,:);
    X{i,3} = [dH_c(i),dH_c(i),dH_c(i)];
end


% c = [0 1];
% [T,n] = size(Ydataset);
% Xfuzzy = [Hmin*u,Hc*u,Hmax*u];
%
% X = cell(T,2);
% for i = 1 : T
%     X{i,1} = 1;
%     X{i,2} = Xfuzzy(i,:);
% end



[xL,xU] = decompose_fuzzy_sets(X);

nalpha = size(xU,3);
A = 0;
B = 0;
for k = 1:nalpha
    A = A + xL(:,:,k)'*xL(:,:,k) + xU(:,:,k)'*xU(:,:,k);
    B = B + xL(:,:,k)' + xU(:,:,k)';
end

t = zeros(n,nalpha);
Ip = eye(size(X,2));

for k = 1:nalpha
    for s = 1:size(Ydataset,2)
        Y = Ydataset(:,s);
        b = A\(B*Y);
        lambda0 = (Y - xU(:,:,k)*b)'*(Y - xU(:,:,k)*b);
        lambda0 = lambda0/T;
        M = A\B;
        C = c * lambda0 * Ip * (M * M') * c';
        t(s,k) = (b' * c') ./ sqrt(C);
    end
end
t = max(t,[],2);
%t = t(:,12);

function [xL,xU] = decompose_fuzzy_sets(X)

N = size(X,1);
p = size(X,2)-1;
nalpha = 30;

tr_mf=@(support,p) max( min( (support-p(1))/(p(2)-p(1)), (p(3)-support)/(p(3)-p(2)) ), 0);

X_mf = cell(N,p+1); % compute the fuzzy sets associated to each dimension

for n=1:N

    for j=2:p+1
        t = linspace(X{n,j}(1),X{n,j}(3),50);
        md = tr_mf(t,X{n,j});
        X_mf{n,j} = [t;md];
    end

end

xU = zeros(N,p+1,nalpha);
xL = zeros(N,p+1,nalpha);
alphas = linspace(0,1,nalpha);
for k = 2:nalpha-1
    for n = 1:N

        j=1;
        xU(n,j,k) = 1;
        xL(n,j,k) = 1;

        for j = 2:p+1
            if ~( X{n,j}(1)==X{n,j}(2) & X{n,j}(2)==X{n,j}(3) ) % to see if the FN is not a singleton
                tmp = X_mf{n,j}(2,:);
                tmp = tmp>=alphas(k);
                tmp2 = find(tmp == 1);
                indU = tmp2(end);
                indL = tmp2(1);
                xU(n,j,k) = X_mf{n,j}(1,indU);
                xL(n,j,k) = X_mf{n,j}(1,indL);
            else
                xU(n,j,k) = X{n,j}(1);
                xL(n,j,k) = X{n,j}(1);
            end
        end


    end
end

for n = 1:N

    j=1;
    xU(n,j,1) = 1;
    xL(n,j,1) = 1;
    xU(n,j,nalpha) = 1;
    xL(n,j,nalpha) = 1;

    for j = 2:p+1
        xU(n,j,1) = X{n,j}(3);
        xL(n,j,1) = X{n,j}(1);
        xU(n,j,nalpha) = X{n,j}(2);
        xL(n,j,nalpha) = X{n,j}(2);
    end

end
