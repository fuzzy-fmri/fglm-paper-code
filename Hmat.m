function H = Hmat( h )
T = length(h);
H = toeplitz(h, [h(1),zeros(1,T-1)]);