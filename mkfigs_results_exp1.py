#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 26 13:16:14 2017
@author: alejandro
"""

import os
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import toeplitz
import matplotlib as mpl
from mpltools import style

def mkfigs(M, save=False, filename='tmp.pdf'):
    fontsize = 12
    fontsize2 = 14

    plt.close('all')
    colors = ['r','g','b','k','c','m','y']
    f = plt.figure(1, figsize=(6,3))
    mpl.rcParams['lines.linewidth'] = 1.5

    #aucs = [ M['auc_glm_oracle'], M['auc_fglm_decomp'], M['auc_fglm_lr'], M['auc_glm'], M['auc_glm_dhrf'], M['auc_glm_dict'], M['auc_glm_wav']]
    #method_label = [ 'GLM oracle', r'$\alpha$-cut FGLM', 'LR-FGLM', 'GLM', 'GLM+deriv', 'SpGLM', 'WP']
    aucs = [ M['auc_glm_oracle'], M['auc_fglm_lr'], M['auc_glm'], M['auc_glm_dhrf'], M['auc_glm_dict'], M['auc_glm_wav']]
    method_label = [ 'GLM oracle', 'LR-FGLM', 'GLM', 'GLM+deriv', 'SpGLM', 'WP']
    n_cases = len(aucs)

    ax = f.add_subplot(111)
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(fontsize)
    for i in range(n_cases):
        ax.plot(M['ttps'].reshape(20,1), aucs[i], color=colors[i], label=method_label[i] )
    ax.axvline(x=5.2, ymin=0, ymax = 1.05, linewidth=2, color='k')
    ax.set_xlim([M['ttps'].min(), M['ttps'].max()])
    ax.set_ylim([0.4,1.001])
    line_color = (0,0,0)
    plt.xlabel('Time-to-peak (s)', fontsize=fontsize)
    plt.ylabel('Area under the ROC curve (AUC)', fontsize=fontsize)
    ax.legend(loc='upper left', bbox_to_anchor=(0.48, 1.), fancybox=True, shadow=True, ncol=1, fontsize=fontsize2)
    ax.xaxis.label.set_color(line_color)
    ax.yaxis.label.set_color(line_color)
    ax.tick_params(axis='x', colors=line_color)
    ax.tick_params(axis='y', colors=line_color)
    plt.tight_layout(pad=0.5,h_pad=0.1, w_pad=0.01)
    if save:
        plt.savefig(filename,dpi=1000)

def get_snrs_durs(files_exp1):
    SNRs = []
    DURs = []
    for f in files_exp1:
        f = f.split('_')
        for s in f:
            if 'SNR' in s:
                s = float(s.replace('SNR','').replace('p','.'))
                SNRs.append( s )
            elif 'dur' in s:
                s = float(s.replace('dur','').replace('.mat',''))
                DURs.append( s )
    SNRs = list(set(SNRs))
    DURs = list(set(DURs))
    SNRs.sort()
    DURs.sort()
    print SNRs
    print DURs
    return SNRs, DURs

def get_filenames(files_exp1, dur=1):
    return sorted([f for f in files_exp1 if 'dur'+str(dur)+'.mat' in f])

files_exp1 = [f for f in os.listdir('results') if f[-4:] in f and 'aucs_varHRF_oversubjects' in f and '.mat' in f]

if True:
    for f in files_exp1:
        M = sio.loadmat(os.path.join('results',f))
        print 'abriendo ',os.path.join('results',f)
        filename = os.path.join('results',f).replace('.mat','.pdf')
        print 'escribiendo ',filename
        mkfigs(M, save=True, filename=filename)
