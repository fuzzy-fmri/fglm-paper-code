clear all, clc, close all

% SNR, duration
TR = 2.5;
T = 121;

datapath = [pwd,'/data/'];

number_of_cases = 10; % number of steps in which the HRF varies
n_of_repetitions = 1000; % number of signals per case
n_of_blocks = 4; % numero de bloques

Ylabels = [zeros(1,n_of_repetitions),ones(1,n_of_repetitions)];
v = linspace(-1,5,number_of_cases); % for data
parameter_index = 1;
timeline = (0:TR:TR*T-TR)';

snrs = [0.2];
durations = [1];
elapsed_time = zeros(7,length(snrs)*length(durations));
exp_count = 0;

for snr = snrs
    for duration = durations

        exp_count = exp_count + 1;
        %------------------------------ making the data

        disp([snr, duration])

        % stimulus function
        inicio = 8;
        if duration == 5
            inicio = 13;
        elseif duration == 1
            inicio = 15;
        end
        fin = inicio+duration;
        u = zeros(T,1);
        u(inicio+1 : fin) = 1;

        %stem(timeline,u_segment)

        % load noise
        j = nifti('noise/msk.nii');
        mask = double(j.dat);
        % show_data(mask,7)
        j = nifti('noise/tmap.nii');
        map = double(j.dat);
        j = nifti('noise/swbold_nodrift.nii');
        Ytmp = double( j.dat );
        [xdim, ydim, zdim, tdim] = size(Ytmp);
        signals = zeros(tdim,xdim*ydim*zdim);
        for t = 1:tdim
            signals(t,:) = reshape(Ytmp(:,:,:,t),1,xdim*ydim*zdim);
        end
        map = reshape(map,1,xdim*ydim*zdim);
        mask = reshape(mask,1,xdim*ydim*zdim);
        noise_mask = zeros(1,xdim*ydim*zdim);
        noise_mask(map>-1 & map<1) = 1;
        noise_mask = noise_mask.*mask;
        % show_data(reshape(noise_mask,xdim,ydim,zdim),7)
        noise_signals_set = signals(:,noise_mask == 1);
        random_indices = randi(sum(noise_mask == 1),1,2*n_of_repetitions);
        noise_signals = noise_signals_set(:,random_indices);
        noise_std = repmat(std( noise_signals ), T, 1);
        noise_mean = repmat(mean( noise_signals ), T, 1);
        noise_signals = noise_signals - noise_mean;
        noise_signals = noise_signals ./ noise_std; 
        %imagesc(noise_signals_set)

        p = [5.4 6 10.8 12 0.35];
        v = linspace(-1,5,number_of_cases);
        hrfs = zeros(T, number_of_cases);
        H = zeros(T, T, number_of_cases);
        Y = zeros(T, number_of_cases);
        for i = 1 : number_of_cases
            p2 = p;
            p2( parameter_index ) = p( parameter_index ) + v(i);
            hrfs(:,i) = hrf_glover(timeline,p2);
            hrfs(:,i) = hrfs(:,i) / max( hrfs(:,i) );
            H(:,:,i) = Hmat(hrfs(:,i));
            Y(:,i) = H(:,:,i) * u;
            Y(:,i) = Y(:,i) / max( Y(:,i) );
        end
        %imagesc(Y)

        last_sample = find( sum(abs(Y),2)>1e-2, 1, 'last' );
        %stem(sum(abs(Y),2)>1e-2)

        Y_segment = Y(1:last_sample,:);
        u_segment = u(1:last_sample,:);
        Y_selection = randi(number_of_cases,[n_of_repetitions n_of_blocks]);
        Y_selection = repmat([10 5 3 1],[n_of_repetitions 1]);
        
        Y = [];
        for i = 1:n_of_repetitions
            tmp = [];
            for j = 1:n_of_blocks
                %tmp = [tmp; (0.7+0.3*rand) * Y_segment(:, Y_selection(i,j) ) ];
                tmp = [tmp; Y_segment(:, Y_selection(i,j) ) ];
            end
            Y = [Y,tmp];
        end
        %imagesc(Y)
        Ymat = zeros( length(u), n_of_repetitions);
        Ymat(1:size(Y,1), :) = Y;
        
%         hold on
%         plot(Y(:,1))
%         pause(1)

        Ymat_std = repmat(std( Ymat ), T, 1);
        Ymat_mean = repmat(mean( Ymat ), T, 1);
        Ymat = Ymat - Ymat_mean;
        Ymat = Ymat ./ Ymat_std;
        Ymat = [zeros(T,n_of_repetitions), Ymat];

        w = 1/snr;
        Ymat_noisy = Ymat + w*noise_signals;

        %imagesc(Ymat)
        %figure, imagesc(Ymat_noisy)

        name = [datapath,'signals_varHRFovertime_SNR',...
            strrep(num2str(snr), '.', 'p'),...
            '_dur',num2str(duration)];

        save(name,'TR', 'u', 'timeline', 'Ymat', 'Ymat_noisy', 'Ylabels')
        
        %------------------------------ tmaps

        name = [datapath,'signals_varHRFovertime_SNR',...
            strrep(num2str(snr), '.', 'p'),...
            '_dur',num2str(duration)];
                
        load(name)

        tic
        t_glm_oracle = glm_oracle(Ymat_noisy, Ymat, Ylabels, 'over_time');
        elapsed_time(1,exp_count) = toc;
        tic
        t_glm = glm(Ymat_noisy,u,TR);
        elapsed_time(2,exp_count) = toc;
        tic
        t_glm_dhrf = glm_dhrf(Ymat_noisy,u,TR);
        elapsed_time(3,exp_count) = toc;
        tic
        t_fglm_lr = glm_fuzzy_lr_fs(Ymat_noisy,u,TR);
        elapsed_time(4,exp_count) = toc;
        tic
        t_fglm_decomp = glm_fuzzy_decomposed_fs(Ymat_noisy,u,TR);
        elapsed_time(5,exp_count) = toc;
        tic
        t_glm_dict = glm_dictionary_timevar(Ymat_noisy, Ylabels, u, timeline, Ymat);
        elapsed_time(6,exp_count) = toc;
        tic
        t_glm_wav = glm_wavelet_timevar(Ymat_noisy);
        elapsed_time(7,exp_count) = toc;
        
        
        name2save = ['tvalues/tvalues_varHRF_overtime_SNR',...
                    strrep(num2str(snr), '.', 'p'),...
                    '_dur',num2str(duration)];
        
        save(name2save,...
            't_glm',...
            't_glm_dhrf',...
            't_fglm_lr',...
            't_fglm_decomp',...
            't_glm_oracle',...
            't_glm_dict',...
            't_glm_wav',...
            'Ylabels')
        
        %------------------------------ writing the results
        
        npts = 1000;
        
        %'t_glm','t_glm_dhrf','t_fglm_lr','t_fglm_decomp',
        %'t_glm_oracle','t_glm_dict','t_glm_wav','Ylabels'        
        name = ['tvalues/tvalues_varHRF_overtime_SNR',...
                    strrep(num2str(snr), '.', 'p'),...
                    '_dur',num2str(duration)];

        load(name)
        
        map = t_glm_oracle(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_glm_oracle,y_glm_oracle] = mrROC(data,gold,npts);
        auc_glm_oracle = abs(trapz(x_glm_oracle,y_glm_oracle));
        
        map = t_glm(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_glm,y_glm] = mrROC(data,gold,npts);
        auc_glm = abs(trapz(x_glm,y_glm));

        map = t_glm_dhrf(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_glm_dhrf,y_glm_dhrf] = mrROC(data,gold,npts);
        auc_glm_dhrf = abs(trapz(x_glm_dhrf,y_glm_dhrf));
        
        map = t_fglm_lr(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_fglm_lr,y_fglm_lr] = mrROC(data,gold,npts);
        auc_fglm_lr = abs(trapz(x_fglm_lr,y_fglm_lr));
        
        %disp([auc_glm_dhrf, auc_fglm_lr])
        
        map = t_fglm_decomp(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_fglm_decomp,y_fglm_decomp] = mrROC(data,gold,npts);
        auc_fglm_decomp = abs(trapz(x_fglm_decomp,y_fglm_decomp));
        
        map = t_glm_dict(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_glm_dict,y_glm_dict] = mrROC(data,gold,npts);
        auc_glm_dict = abs(trapz(x_glm_dict,y_glm_dict));
        
        map = t_glm_wav(:);
        tnoise = map(Ylabels == 0);
        t = map(Ylabels == 1);
        gold = [zeros(length(tnoise),1);ones(length(t),1)];
        data = [tnoise;t];
        [x_glm_wav,y_glm_wav] = mrROC(data,gold,npts);
        auc_glm_wav = abs(trapz(x_glm_wav,y_glm_wav));
        
        name2save = ['results/rocs_varHRF_overtime_SNR',...
            strrep(num2str(snr), '.', 'p'),...
            '_dur',num2str(duration)];
        
        save(name2save,...
            'auc_glm_oracle','x_glm_oracle','y_glm_oracle',...
            'auc_glm','x_glm','y_glm',...
            'auc_glm_dhrf','x_glm_dhrf','y_glm_dhrf',...
            'auc_fglm_lr','x_fglm_lr','y_fglm_lr',...
            'auc_fglm_decomp','x_fglm_decomp','y_fglm_decomp',...
            'auc_glm_dict','x_glm_dict','y_glm_dict',...
            'auc_glm_wav','x_glm_wav','y_glm_wav',...
            'elapsed_time')
        
        figure(1)
        plot(x_glm_oracle,y_glm_oracle,'b')
        hold on
        plot(x_glm,y_glm,'k')
        plot(x_glm_dhrf,y_glm_dhrf,'y')
        plot(x_fglm_lr,y_fglm_lr,'r')
        plot(x_fglm_decomp,y_fglm_decomp,'g')
        plot(x_glm_dict,y_glm_dict,'m')
        plot(x_glm_wav,y_glm_wav,'c')
        legend({'GLM oracle','GLM','GLM + HRF derivatives',...
            'fuzzy GLM-LR','fuzzy GLM-\alpha','Sparse GLM','Wavelets'},...
            'Location','SouthWest')
        title(strrep(name2save, '_', ' '))
        xlabel('True positive rate','fontsize',24)
        ylabel('False positive rate','fontsize',24)
        grid on
        axis([0 1 0 1.01])
        set(gca,'fontsize',22)
        hold off
        
        save_fig(name2save)

    end
end