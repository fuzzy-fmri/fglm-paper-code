function show_data(im, rows)
% show_actmap2(im1, im2, imscale, mapscale, rows)

h.xdim=size(im,1);
h.ydim=size(im,2);
h.zdim=size(im,3);
cols=ceil(h.zdim/rows);
im = get_BigMat(im,h,rows,cols);

imagesc(im)
colormap(gray)
axis xy

% plot_map(anat,map,u)

function BigMat = get_BigMat(im,h,rows,cols)

BigMat = [];
for r = 1:rows
	Mrow = [];
	for c = 1:cols
		sl = c + cols*(r-1);
		if sl <= h.zdim
			Mrow = [Mrow  im(:,:,sl)'];
		else
			Mrow = [Mrow  zeros(h.ydim, h.xdim)];
        end
	end
	BigMat = [BigMat; Mrow];
end
