function [fpr,tpr] = mrROC(data,gold,ptsROC)

if mean(data(gold==0))>mean(data(gold==1))
    gold = 1-gold;
end

k=0;
fpr = zeros(ptsROC,1);
tpr = zeros(ptsROC,1);
for th = linspace(min(data),max(data),ptsROC)
    k=k+1;
    bin = data>=th;
    fpr(k)=sum(~gold&bin)/sum(~gold);
    tpr(k)=sum(gold&bin)/sum(gold);
end