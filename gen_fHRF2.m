function [Hc,Hmin,Hmax] = gen_fHRF2(T, sampling_rate)
p = [5.4 6 10.8 12 0.35];
timeline = (0:sampling_rate:sampling_rate*T-sampling_rate)';
v1 = linspace(-1,5,100);
v2 = linspace(-3,3,100);
parameter_index1 = 1;
parameter_index2 = 3;
H = zeros(T, T, length(v1)*length(v2));

k = 0;
for i = 1:length(v1)
    for j = 1:length(v2)
        k=k+1;
        p2 = p;
        p2( parameter_index1 ) = p2( parameter_index1 ) + v1(i);
        p2( parameter_index2 ) = p2( parameter_index2 ) + v2(j);
        h = hrf_for_fuzzy(timeline, p2);
        H(:,:,i) = Hmat_for_fuzzy( h );
    end
end
Hmin = min(H,[],3);
Hmax = max(H,[],3);
Hc = Hmat_for_fuzzy( hrf_for_fuzzy(timeline,p) );

function h = hrf_for_fuzzy(timeline,p)
tau1 = p(1); delta1 = p(2);
tau2 = p(3); delta2 = p(4);
c = p(5);
gamma1 = ((timeline/tau1).^delta1).*exp(-(delta1/tau1)*(timeline-tau1));
gamma2 = c*((timeline/tau2).^delta2).*exp(-(delta2/tau2)*(timeline-tau2));
h = gamma1 - gamma2;
h = h/max(h);

function H = Hmat_for_fuzzy( h )
T = length(h);
H = toeplitz(h, [h(1),zeros(1,T-1)]);
