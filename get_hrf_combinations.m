function [hrfs,disps,ttps] = get_hrf_combinations
TR = 1;
T = 301;
tau1s = 5.4 + linspace(-1,5,100);
tau2s = 10.8 + linspace(-3,3,100);
timeline = (0:TR:TR*T-TR)';
hrfs = [];
ttps = [];
disps = [];
for tau1 = tau1s
    for tau2 = tau2s
        p = [5.4 6 10.8 12 0.35];
        p(1) = tau1; p(3) = tau2;
        h = hrf_glover(timeline,p);
        [ttp, disp] = get_features(h, timeline);
        ttps = [ttps, ttp];
        disps = [disps, disp];        
        hrfs = [hrfs, h];
    end
end


function [ttp, disp] = get_features(h, timeline)
ttp = timeline( find( h == max(h) ) );
half_max = max(h) / 2;
timeline_segment = timeline(h >= half_max);
p1 = min(timeline_segment);
p2 = max(timeline_segment);
disp = p2 - p1;