function Y_nodrift = drift_correction(data)

% this software wants it 2D as (time, voxels) so we reshape as
ordermax_drift = 3; % Default maximum polynomial drift order
[L, numVoxels] = size(data);
Y_nodrift = zeros(size(data));
NFFT = (2^nextpow2(L)); % number of FFT points
% setupPolyDrift
polydrift_t = offset_drift_orthogonal(ordermax_drift, L);
polydrift = timeFreqSig(polydrift_t, 'time', NFFT, L);            
Lzeros = zeros(L,1);     
% loop through voxels
for n=1:numVoxels
    y_t = data(:,n);
    % do not process voxels containing null data
    if ~isequaln(y_t, Lzeros)
        y_t = y_t - mean(y_t);
        y = timeFreqSig(y_t, 'time', NFFT, L);
        order_polydrift = getRegressionModelOrder(y.t, polydrift.t, 1);
        drift.t = polydrift.t(:,1:order_polydrift);
        drift.k = polydrift.k(:,1:order_polydrift);
        y_nodrift = y.t - drift.t*(drift.t\y.t);
        Y_nodrift(:,n) = y_nodrift; 
    end
end

function [modelOrder] = getRegressionModelOrder(data, regressorVariables, groupSize, subGroupPickElts)
% getRegressionModelOrder.m
%
%
% groupSize is an optional input; if regressorVariables is divided into groups (e.g. for
% coordinates or basis sets etc) it specifies the number of variable
% sub-components in each group.
% 
% If groupSize is specified, it returns [order] as the number of selected
% groups instead of the total number of variables 
% i.e. size(regressorVariables,2) = groupSize * (number of groups)
%
%
% Ben Cassidy 2012-03-29

n=length(data);
if (~exist('groupSize', 'var') || groupSize ==1 || ~exist('subGroupPickElts', 'var'))
    groupSize =1;
    subGroupPickElts = 1;
end

if groupSize ==1
    numberOfGroups =size(regressorVariables,2);
else
    numberOfGroups =size(regressorVariables,2)/groupSize;
end

BIC_order = zeros(numberOfGroups,1)*NaN;
for k=1:numberOfGroups

    if groupSize == 1
        X = regressorVariables(:,1:k);
    else
        X = []; %init.
        % lazy but it works and is understandable: iteratively select  and
        % concatenate the first columns of each group, then 2nd, 3rd etc.
        for index = 1:k
            X = [X, regressorVariables(:,(1:subGroupPickElts)+(index-1)*groupSize)];
        end
    end

    b = X\data;
    res = data- X*b;
    residual_var = (norm(res))^2/(n - k*subGroupPickElts);
    
    BIC_order(k) = n*(log(residual_var)) + (k)*log(n);
end
    
[~,modelOrder] = min(BIC_order);

function [outSig] = timeFreqSig(sig, type, NFFT, L)
% timeFreqSig.m
%
% Input column-vector signal matrix
% Outputs a struct with the time-domain and frequency-domain signals of the
% input.  Outputs normalised fft and ifft values, assumes same for inputs.
%
%   INPUTS:
% sig       : double : time or frequency signal to convert
% type      : string : type of sig, either 'time' or 'freq'
% NFFT      : int : number of frequency samples
% L         : int : number of time samples
%
%   OUTPUTS:
% outSig.t : double : time-domain signal
% outSig.k : double : frequency-domain signal
%
% Ben Cassidy 2012-03-29

switch lower(type)
    case 'time'
        outSig.t = sig;
        outSig.k = (1/sqrt(NFFT))*fft(sig, NFFT);
    case 'freq'
        outSig.t = (sqrt(NFFT))*ifft(sig, 'symmetric');
        outSig.t = outSig.t(1:L,:);
        outSig.k = sig;
    otherwise
        error('Invalid input type, should be "time" or "freq"')
end

function [basis] =  offset_drift_orthogonal(order, length, noConst)
% offset_drift_orthogonal.m
%
% Construct polynomial drift regressor matrix, shape =(length x (order +1))
%
% Optional input 'noConst' returns basis without constant (dc) vector
%
% Ben Cassidy 2012-03-29

T=length;

if (mod(T,2) ==1)
    t0 = ones(T,1);
    t1 = (((1:T)-T/2).');
    t2 = (((1:T)-T/2).').^2;
    t3 = (((1:T)-T/2).').^3;
else
    t0 = ones(T,1);
    t1 = (((1:T)-(T-1)/2).');
    t2 = (((1:T)-(T-1)/2).').^2;
    t3 = (((1:T)-(T-1)/2).').^3;
end

% make b0 independent of first order term
t1_res = t1 - t0;

% estimate 2nd order error
y=t2;
X = [t0 t1];
alpha= X\y;
t2_reg = X*alpha;

t2_res = t2 - t2_reg;

% repeat for 3rd order
y=t3;
X = [t0 t1_res t2_res];
alpha = X\y;
t3_reg = X*alpha;

t3_res = t3 - t3_reg;

switch order
    case 0
        basis = [t0];
    case 1
        basis = [t0 t1_res];
    case 2
        basis = [t0 t1_res t2_res];
    case 3
        basis = [t0 t1_res t2_res t3_res];
    otherwise
        basis = [t0 t1_res t2_res t3_res];
        printf('Using default o(3) drift basis set')
end

basis = basis./repmat(max(basis), T,1);
try basis(:,2:2:end) = -basis(:,2:2:end); end 
try if noConst, basis = basis(:,2:end); end, end