
import os
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import toeplitz
import matplotlib as mpl
from mpltools import style

filenames = ['rocs_varHRF_overtime_SNR0p2_dur1.mat']

SNRs = [0.2]
DURs = [1]


#'auc_glm_oracle','x_glm_oracle','y_glm_oracle',...
#'auc_glm','x_glm','y_glm',...
#'auc_glm_dhrf','x_glm_dhrf','y_glm_dhrf',...
#'auc_fglm_lr','x_fglm_lr','y_fglm_lr',...
#'auc_fglm_decomp','x_fglm_decomp','y_fglm_decomp',...
#'auc_glm_dict','x_glm_dict','y_glm_dict',...
#'auc_glm_wav','x_glm_wav','y_glm_wav'



colors = ['r','g','b','k','c','m','y']

methods = ['glm_oracle',
           'fglm_decomp',
           'fglm_lr',
           'glm',
           'glm_dhrf',
           'glm_dict',
           'glm_wav']

methods_labels = [ 'GLM oracle',
                r'$\alpha$-FGLM',
                'LR-FGLM',
                'GLM',
                'GLM+deriv',
                'SpGLM',
                'WP']

methods = ['glm_oracle',
           'fglm_lr',
           'glm',
           'glm_dhrf',
           'glm_dict',
           'glm_wav']

methods_labels = [ 'GLM oracle',
                'LR-FGLM',
                'GLM',
                'GLM+deriv',
                'SpGLM',
                'WP']



for f in filenames:
    print f
    M = sio.loadmat(os.path.join('results', f))
    #print M['auc_glm_oracle']
    filename = os.path.join('results',f).replace('.mat','.pdf')

#    style.use('ggplot')
#    style.use('ieee.transaction')
    fontsize = 12
    fontsize2 = 14

    plt.close('all')
    f = plt.figure(1, figsize=(4,4))
    mpl.rcParams['lines.linewidth'] = 1.5

    ax = f.add_subplot(111)

    for i,method in enumerate(methods):

        x = M['x_'+method]
        y = M['y_'+method]
        ax.plot(x, y, color=colors[i], label=methods_labels[i] )

        line_color = (0,0,0)
        plt.xlabel('False Positive rate', fontsize=fontsize)
        plt.ylabel('True Positive rate', fontsize=fontsize)
        ax.legend(loc='upper left', bbox_to_anchor=(0.48, .7), fancybox=True, shadow=True, ncol=1, fontsize=fontsize2)
        ax.xaxis.label.set_color(line_color)
        ax.yaxis.label.set_color(line_color)
        ax.tick_params(axis='x', colors=line_color)
        ax.tick_params(axis='y', colors=line_color)

        plt.axis('square')
        plt.tight_layout(pad=0.5,h_pad=0.1, w_pad=0.01)

        ax.set_xlim([-0.01, 1.01])
        ax.set_ylim([-0.01, 1.01])

    plt.savefig(filename,dpi=1000)


for f in filenames:
    print f
    M = sio.loadmat(os.path.join('results', f))

    header = '&'.join( methods_labels ) + '\\'
    line = []

    print header

    for i,method in enumerate(methods):

        auc = M['auc_'+method]
        line.append( str(round(auc[0][0],2)) )
    line = '&'.join( line ) + ' \\'

    print line
