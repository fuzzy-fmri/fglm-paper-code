function t = glm_wavelet_timevar(Y)

t = wavelet_packet(Y);

function actlevel = wavelet_packet(Y)

J = 3;
A = [];

N = size(Y,2);

for i = 1:N
    wpt = wpdec(Y(:,i),J,'db1','shannon');
    a = [];
    for j = 1:J
        for k=0:2^j-1
            atmp = wpcoef(wpt,[j k])';
            a = [a, atmp];
        end
    end
    A = [A;a];
end

coeff = pca(A); 
X = (coeff(:,1:2)' * A')'; 

[c,U] = fcm(X,2);
U = U';

if norm(c(1,:)) > norm(c(2,:))
    i = 1;
else
    i = 2;
end

actlevel = U(:,i);







% % % function varargout = wpdec(x,depth,wname,type_ent,parameter) 
% % % %WPDEC Wavelet packet decomposition 1-D. 
% % % %   T = WPDEC(X,N,'wname',E,P) returns a wptree object T 
% % % %   corresponding to a wavelet packet decomposition 
% % % %   of the vector X, at level N, with a 
% % % %   particular wavelet ('wname', see WFILTERS). 
% % % %   E is a string containing the type of entropy (see WENTROPY): 
% % % %   E = 'shannon', 'threshold', 'norm', 'log energy', 'sure', 'user' 
% % % %   P is an optional parameter: 
% % % %        'shannon' or 'log energy' : P is not used 
% % % %        'threshold' or 'sure'     : P is the threshold (0 <= P) 
% % % %        'norm' : P is a power (1 <= P) 
% % % %        'user' : P is a string containing the name 
% % % %                 of an user-defined function. 
% % % % 
% % % %   T = WPDEC(X,N,'wname') is equivalent to 
% % % %   T = WPDEC(X,N,'wname','shannon'). 
% % % % 
% % % %   See also WAVEINFO, WENTROPY, WPDEC2, WPREC, WPREC2. 
% % %  
% % % %   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 12-Mar-96. 
% % % %   Last Revision: 21-May-2003. 
% % % %   Copyright 1995-2004 The MathWorks, Inc. 
% % % % $Revision: 1.12.4.2 $ 
% % %  
% % % %--------------% 
% % % % OLD VERSION  % 
% % % %--------------% 
% % % %   [T,D] = WPDEC(X,N,'wname',E,P) returns a tree structure T 
% % % %   and a data structure D (see MAKETREE), corresponding to a 
% % % %   wavelet packet decomposition of the vector X, at level N, 
% % % %   with a particular wavelet ('wname', see WFILTERS). 
% % % %   E is a string containing the type of entropy (see WENTROPY): 
% % % %   E = 'shannon', 'threshold', 'norm', 'log energy', 'sure, 'user' 
% % % %   P is an optional parameter: 
% % % %        'shannon' or 'log energy' : P is not used 
% % % %        'threshold' or 'sure'     : P is the threshold (0 <= P) 
% % % %        'norm' : P is a power (1 <= P < 2) 
% % % %        'user' : P is a string containing the name 
% % % %                 of an user-defined function. 
% % % % 
% % % %   [T,D] = WPDEC(X,N,'wname') is equivalent to 
% % % %   [T,D] = WPDEC(X,N,'wname','shannon'). 
% % % % 
% % % %   See also MAKETREE, WAVEINFO, WDATAMGR, WENTROPY 
% % % %            WPDEC2, WTREEMGR. 
% % %  
% % % % Check arguments. 
% % % nbIn = nargin; 
% % % if nbIn < 3 ,    error('Not enough input arguments.'); 
% % % elseif nbIn==3 , parameter = 0.0; type_ent = 'shannon'; 
% % % elseif nbIn==4 , parameter = 0.0; 
% % % end 
% % % if strcmp(lower(type_ent),'user') 
% % %     if ~ischar(parameter) 
% % %         error('*** Invalid function name for user entropy ***'); 
% % %     end 
% % % end 
% % %  
% % % % Tree Computation 
% % % if nargout==1    % NEW VERSION 
% % %     order = 2; 
% % %     varargout{1} = wptree(order,depth,x,wname,type_ent,parameter); 
% % %  
% % % else	         % OLD VERSION 
% % % 	verWTBX = wtbxmngr('version','nodisp'); 
% % % 	if ~isequal(verWTBX,'V1') 
% % % 		WarnString = strvcat(... 
% % % 					'Warning: The number of output arguments for the wpdec', ... 
% % % 			        'function has changed and this calling syntax is obsolete.',  ... 
% % % 					'Please type "help wtbxmngr" at the MATLAB prompt', ... 
% % % 					'for more information.' ... 
% % % 					); 
% % % 		DlgName = 'Warning Dialog'; 
% % % 		wwarndlg(WarnString,DlgName,'bloc'); 
% % % 		varargout = cell(1,nargout);  
% % % 		return 
% % % 	end 
% % %     [varargout{1},varargout{2}] = owpdec(x,depth,wname,type_ent,parameter); 
% % % end 
% % %  
% % %  
% % % %=============================================================================% 
% % % % INTERNAL FUNCTIONS 
% % % %=============================================================================% 
% % % %-----------------------------------------------------------------------------% 
% % % function [Ts,Ds] = owpdec(x,depth,wname,type_ent,parameter) 
% % % %WPDEC Wavelet packet decomposition 1-D. 
% % %  
% % % [mi,ind] = min(size(x)); 
% % % if (ind==1) & (mi<2) 
% % %     x_shape = 'r'; 
% % % elseif (ind==2) & (mi<2) 
% % %     x_shape = 'c'; 
% % % else 
% % %     error('Invalid argument value.'); 
% % % end 
% % %  
% % % % Initialization 
% % % %%%%%%%%%%%%%%%% 
% % % order       = 2; 
% % % [Ts,nbtn]   = maketree(order,depth,order/2); 
% % % sizes       = zeros(1,depth+1); 
% % % Ds          = x(:)'; 
% % % ent         = zeros(1,(order*nbtn-1)/(order-1)); 
% % % tmp         = NaN; 
% % % ent_opt     = tmp(ones(size(ent))); 
% % %  
% % % lx          = length(x); 
% % % sizes(1)    = lx; 
% % % ent(1)      = wentropy(x,type_ent,parameter); 
% % %  
% % % % Tree computation 
% % % %%%%%%%%%%%%%%%%%% 
% % % [Lo_D,Hi_D] = wfilters(wname,'d'); 
% % % n = 2; 
% % % for k=0:depth-1 
% % %     beg = 1; 
% % %     for p=0:order^k-1 
% % %         x        = Ds(beg:beg+lx-1); 
% % %         [a,d]    = dwt(x,Lo_D,Hi_D); 
% % %         Ds       = [Ds(1:beg-1) a d Ds(beg+lx:end)]; 
% % %         ent(n)   = wentropy(a,type_ent,parameter); 
% % %         ent(n+1) = wentropy(d,type_ent,parameter); 
% % %         beg      = beg+2*length(a); 
% % %         n        = n+order; 
% % %     end 
% % %     lx         = length(a); 
% % %     sizes(k+2) = lx; 
% % % end 
% % % Ts = wtreemgr('winfo',Ts,lx*ones(1,nbtn)); 
% % %  
% % % % Writing wavelet and entropy 
% % % %%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% % % Ds = wdatamgr('write',Ds,sizes,x_shape,... 
% % %                 ent,ent_opt,parameter,type_ent,wname,order); 
% % % %-----------------------------------------------------------------------------% 
% % % %=============================================================================% 
% % % 
% % % 
% % % function x = wpcoef(wpt,node) 
% % % %WPCOEF Wavelet packet coefficients. 
% % % %   X = WPCOEF(T,N) returns the coefficients associated 
% % % %   with the node N of the wavelet packet tree T. 
% % % %   If N doesn't exist, X = []; 
% % % % 
% % % %   X = WPCOEF(T) is equivalent to X = WPCOEF(T,0)  
% % % % 
% % % %   See also WPDEC, WPDEC2. 
% % %  
% % % %   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 12-Mar-96. 
% % % %   Last Revision: 14-May-2003. 
% % % %   Copyright 1995-2004 The MathWorks, Inc. 
% % % %   $Revision: 1.4.4.2 $  $Date: 2004/03/15 22:39:18 $ 
% % %  
% % % if nargin==1, node = 0; end 
% % % [nul,x] = nodejoin(wpt,node); 
% % % 
% % % 
% % % 
% % % 
% % % function t = wptree(order,depth,x,wname,type_ent,parameter,userdata) 
% % % %WPTREE Constructor for the class WPTREE. 
% % % %   T = WPTREE(ORDER,DEPTH,X,WNAME,ENT_TYPE,ENT_PAR) returns 
% % % %   a complete wavelet packet tree T. 
% % % % 
% % % %   ORDER is an integer representing the order of the tree 
% % % %   (number of "children" of each non terminal node). It must 
% % % %   be equal to 2 or 4. 
% % % % 
% % % %   If ORDER = 2, T is a WPTREE object corresponding to a  
% % % %   wavelet packet decomposition of the vector (signal) X, 
% % % %   at level DEPTH with a particular wavelet WNAME. 
% % % % 
% % % %   If ORDER = 4, T is a WPTREE object corresponding to a  
% % % %   wavelet packet decomposition of the matrix (image) X, 
% % % %   at level DEPTH with a particular wavelet WNAME. 
% % % % 
% % % %   ENT_TYPE is a string containing the type of entropy 
% % % %   and ENT_PAR is an optional parameter used for entropy 
% % % %   computation (see WENTROPY, WPDEC or WPDEC2 for more  
% % % %   information). 
% % % % 
% % % %   T = WPTREE(ORDER,DEPTH,X,WNAME) is equivalent to  
% % % %   T = WPTREE(ORDER,DEPTH,X,WNAME,'shannon'). 
% % % % 
% % % %   With T = WPTREE(ORDER,DEPTH,X,WNAME,ENT_TYPE,ENT_PAR,USERDATA) 
% % % %   you may set a userdata field. 
% % % % 
% % % %   The function WPTREE returns a WPTREE object. 
% % % %   For more information on object fields, type: help wptree/get.   
% % % % 
% % % %   See also DTREE, NTREE. 
% % %  
% % % %   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 15-Oct-96. 
% % % %   Last Revision: 22-May-2003. 
% % % %   Copyright 1995-2004 The MathWorks, Inc. 
% % % %   $Revision: 1.7.4.2 $  $Date: 2004/03/15 22:39:27 $ 
% % %  
% % % %=============================================== 
% % % % Class WPTREE (parent objects: DTREE) 
% % % % Fields: 
% % % %   dtree - Parent object. 
% % % %   wavInfo - Structure (wavelet infos) 
% % % %     wavName : Wavelet Name. 
% % % %     Lo_D    : Low Decomposition filter 
% % % %     Hi_D    : High Decomposition filter 
% % % %     Lo_R    : Low Reconstruction filter 
% % % %     Hi_R    : High Reconstruction filter 
% % % % 
% % % %   entInfo - Structure (entropy infos) 
% % % %     entName : Entropy Name 
% % % %     entPar  : Entropy Parameter 
% % % %   ----------------------------------------- 
% % % %   allNI - Array(nbnode,5)  <---  in DTREE 
% % % %     [ind,size,ent,ento] 
% % % %          ind  = indice 
% % % %          size = size of data 
% % % %          ent  = Entropy 
% % % %          ento = Optimal Entropy 
% % % %=============================================== 
% % %  
% % % % Check arguments. 
% % % %----------------- 
% % % nbIn = nargin; 
% % % switch nbIn 
% % %   case 0 % Dummy. Only for loading object! 
% % %       order = 2 ; depth = 0; x = 1;  wname = 'db1'; 
% % %       userdata  = []; parameter = 0.0; type_ent  = 'shannon'; 
% % %   case {1,2,3} , error('Invalid number of input arguments.'); 
% % %   case 4 , userdata  = []; parameter = 0.0; type_ent  = 'shannon'; 
% % %   case 5 , userdata  = []; parameter = 0.0; 
% % %   case 6 , userdata  = []; 
% % %   case 7 , 
% % %   otherwise , error('Too many input arguments.'); 
% % % end 
% % % if strcmp(lower(type_ent),'user') 
% % %     if ~ischar(parameter) 
% % %         error('Invalid function name for user entropy.'); 
% % %     end 
% % %     type_ent  = ['user' '&' parameter]; 
% % %     parameter = 0.0; 
% % % end 
% % %  
% % % % Tree creation. 
% % % %--------------- 
% % % d = dtree(order,depth,x,'spflg','notexpand',[],userdata); 
% % %  
% % % % Wavelet infos. 
% % % %--------------- 
% % % t.wavInfo.wavName = wname; 
% % % [ t.wavInfo.Lo_D,t.wavInfo.Hi_D, ... 
% % %   t.wavInfo.Lo_R,t.wavInfo.Hi_R ] = wfilters(wname); 
% % %  
% % % % Entropy infos. 
% % % %--------------- 
% % % t.entInfo.entName = type_ent; 
% % % t.entInfo.entPar  = parameter; 
% % %  
% % % % Built object. 
% % % %--------------- 
% % % t = class(t,'wptree',d); 
% % % t = set(t,'wtboInfo',class(t)); 
% % % t = expand(t); 
% % % 
% % % 
% % % 
% % % 
% % % function Ts = nodejoin(Ts,node)
% % % %NODEJOIN Recompose node.
% % % %   T = NODEJOIN(T,N) returns the modified tree structure T
% % % %   corresponding to a recomposition of the node N.
% % % %
% % % %   The nodes are numbered from left to right and
% % % %   from top to bottom. The root index is 0.
% % % %
% % % %   T = NODEJOIN(T) is equivalent to T = NODEJOIN(T,0).
% % % %
% % % %   See also MAKETREE, NODESPLT, WTREEMGR.
% % %  
% % % %   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 12-Mar-96.
% % % %   Last Revision: 05-May-1999.
% % % %   Copyright 1995-2002 The MathWorks, Inc.
% % % % $Revision: 1.11 $
% % %  
% % % % Check arguments.
% % % if errargn(mfilename,nargin,[1 2],nargout,[0 1]), error('*'); end
% % % if nargin == 1, node = 0; end
% % % 
% % % 
% % % 
% % % 
% % % 
% % % 
% % % 
% % % 
% % % 
% % % 
% % % 
