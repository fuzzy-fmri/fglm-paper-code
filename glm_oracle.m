function t = glm_oracle(Y, Yreal, labels_vec, op)
%t = glm_oracle(Y, Yreal, labels, 'over_subjects')

c = [0 1];
[T,N] = size(Y);
t = zeros(N,1);

if strcmp(op,'over_subjects')

    for i = 1:N

        l = labels_vec(i);
        if l==0
            l = randi(size(Yreal,2));
        end
        X = [ones(T,1),Yreal(:,l)];
        y = Y(:,i);
        p = 2;
        xtx_inv = pinv(X);
        df = T-p+1;

        beta_est = xtx_inv*y;
        xtx_inv = inv(X'*X);
        var_est = ((y-X*beta_est)'*(y-X*beta_est))/T;
        vCon = c * var_est * eye(p) * xtx_inv * c';
        t(i) = (beta_est' * c') ./ sqrt(vCon)';

    end    
    t(isnan(t)) = 0;
    t(isinf(t)) = 0;
    
elseif strcmp(op,'over_time')
    
    Yact = Yreal(:, labels_vec==1);    
    
    for i = 1:N

        l = labels_vec(i);
        if l==0
            X = [ones(T,1),Yact(:, randi(size(Yact,2)) )];
        else
            X = [ones(T,1),Yreal(:, i)];
        end
        
        y = Y(:,i);
        p = 2;
        xtx_inv = pinv(X);
        df = T-p+1;

        beta_est = xtx_inv*y;
        xtx_inv = inv(X'*X);
        var_est = ((y-X*beta_est)'*(y-X*beta_est))/T;
        vCon = c * var_est * eye(p) * xtx_inv * c';
        t(i) = (beta_est' * c') ./ sqrt(vCon)';

    end    
    t(isnan(t)) = 0;
    t(isinf(t)) = 0;
    
end
    
        