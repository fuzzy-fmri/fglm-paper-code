clear all, clc, close all

% SNR, duration
TR = 2.5;
T = 121;
fastop = 0;

datapath = [pwd,'/data/'];

number_of_cases = 20; % number of steps in which the HRF varies
n_of_repetitions = 1000; % number of signals per case
parameter_index = 1;
timeline = (0:TR:TR*T-TR)';

snrs = [0.2];
durations = [1];
elapsed_time = zeros(7,length(snrs)*length(durations));
exp_count = 0;

for snr = snrs
    for duration = durations

        exp_count = exp_count + 1;
        %------------------------------ making the data

        % stimulus function
        inicio = 10 : 20 : T;
        fin = inicio+duration;
        u = zeros(T,1);
        for k = 1:length(inicio)
            u(inicio(k)+1 : fin(k)) = 1;
        end

        % load noise
        j = nifti('noise/msk.nii');
        mask = double(j.dat);
        % show_data(mask,7)
        j = nifti('noise/tmap.nii');
        map = double(j.dat);
        j = nifti('noise/swbold_nodrift.nii');
        Ytmp = double( j.dat );
        [xdim, ydim, zdim, tdim] = size(Ytmp);
        signals = zeros(tdim,xdim*ydim*zdim);
        for t = 1:tdim
            signals(t,:) = reshape(Ytmp(:,:,:,t),1,xdim*ydim*zdim);
        end
        map = reshape(map,1,xdim*ydim*zdim);
        mask = reshape(mask,1,xdim*ydim*zdim);
        noise_mask = zeros(1,xdim*ydim*zdim);
        noise_mask(map>-1 & map<1) = 1;
        noise_mask = noise_mask.*mask;
        % show_data(reshape(noise_mask,xdim,ydim,zdim),7)
        noise_signals_set = signals(:,noise_mask == 1);

        p = [5.4 6 10.8 12 0.35];
        v = linspace(-1,5,number_of_cases);
        hrfs = zeros(T, number_of_cases);
        H = zeros(T, T, number_of_cases);
        Y = zeros(T, number_of_cases);
        ttps = []; % vector of time-to-peaks
        Ymat = []; % data matrix (noiseless)
        Ylabels = [];
        for i = 1 : number_of_cases

            p2 = p;
            p2( parameter_index ) = p( parameter_index ) + v(i);
            hrfs(:,i) = hrf_glover(timeline,p2);
            hrfs(:,i) = hrfs(:,i) / max( hrfs(:,i) );
            H(:,:,i) = Hmat(hrfs(:,i));
            Y(:,i) = H(:,:,i) * u;
            Ymat = [Ymat, repmat( Y(:,i), 1, n_of_repetitions)];
            Ylabels = [Ylabels,i*ones(1, n_of_repetitions)];

            % time-to-peaks
            t_ttp = (0:0.1:100)';
            h_ttp = hrf_glover(t_ttp, p2);
            [m,ind_ttp] = max(h_ttp);
            ttps = [ttps,t_ttp(ind_ttp)];

        end

        Ymat_std = repmat(std( Ymat ), T, 1);
        Ymat_mean = repmat(mean( Ymat ), T, 1);
        Ymat = Ymat - Ymat_mean;
        Ymat = Ymat ./ Ymat_std;

        random_indices = randi(sum(noise_mask == 1),1,length(Ylabels));
        noise_signals = noise_signals_set(:,random_indices);
        noise_std = repmat(std( noise_signals ), T, 1);
        noise_mean = repmat(mean( noise_signals ), T, 1);
        noise_signals = noise_signals - noise_mean;
        noise_signals = noise_signals ./ noise_std;

        w = 1/snr;
        Ymat_noisy = Ymat + w*noise_signals;

        % add signals under the null hypothesis
        Ymat = [zeros(T, n_of_repetitions), Ymat];
        Ymat_noisy = [zeros(T, n_of_repetitions), Ymat_noisy];
        Ylabels = [zeros(1, n_of_repetitions),Ylabels];

        random_indices = randi(sum(noise_mask == 1),1,n_of_repetitions);
        noise_signals_null = noise_signals_set(:,random_indices);
        noise_std = repmat(std( noise_signals_null ), T, 1);
        noise_mean = repmat(mean( noise_signals_null ), T, 1);
        noise_signals_null = noise_signals_null - noise_mean;
        noise_signals_null = noise_signals_null ./ noise_std;

        Ymat_noisy(:,Ylabels == 0) = w*noise_signals_null;


        name = [datapath,'signals_varHRFoversubjs_SNR',...
            strrep(num2str(snr), '.', 'p'),...
            '_dur',num2str(duration)];

        save(name,'TR', 'ttps', 'u', 'Y', 'timeline', ...
            'Ymat', 'Ymat_noisy', 'Ylabels')

        %------------------------------ tmaps

        name = [datapath,'signals_varHRFoversubjs_SNR',...
        strrep(num2str(snr), '.', 'p'),'_dur',num2str(duration)];

        load(name)

        tic
        t_glm_oracle = glm_oracle(Ymat_noisy, Y, Ylabels, 'over_subjects');
        elapsed_time(1,exp_count) = toc;
        tic
        t_glm = glm(Ymat_noisy,u,TR);
        elapsed_time(2,exp_count) = toc;
        tic
        t_glm_dhrf = glm_dhrf(Ymat_noisy,u,TR);
        elapsed_time(3,exp_count) = toc;
        tic
        t_fglm_lr = glm_fuzzy_lr_fs(Ymat_noisy,u,TR);
        elapsed_time(4,exp_count) = toc;
        tic
        t_fglm_decomp = glm_fuzzy_decomposed_fs(Ymat_noisy,u,TR);
        elapsed_time(5,exp_count) = toc;
        if ~fastop
            tic
            t_glm_dict = glm_dictionary(Ymat_noisy, Ylabels, u, timeline, Y);
            elapsed_time(6,exp_count) = toc;
            tic
            t_glm_wav = glm_wavelet(Ymat_noisy, Ylabels);
            elapsed_time(7,exp_count) = toc;
        else
            t_glm_dict = [];
            t_glm_wav = [];
        end
        name2save = ['tvalues/tvalues_varHRF_oversubjects_SNR',...
                        num2str(snr),'_dur',num2str(duration),'.mat'];

        save(name2save,...
            't_glm',...
            't_glm_dhrf',...
            't_fglm_lr',...
            't_fglm_decomp',...
            't_glm_oracle',...
            't_glm_dict',...
            't_glm_wav',...
            'ttps',...
            'Ylabels')

        %------------------------------ writing the results

        npts = 1000;

        %'t_glm','t_glm_dhrf','t_fglm_yoon','t_fglm_decomp','t_glm_oracle',
        %'t_glm_dict','t_glm_wav','x','Ylabels'
        name = ['tvalues/tvalues_varHRF_oversubjects_SNR',...
                    num2str(snr),'_dur',num2str(duration),'.mat'];

        load(name)

        n_of_cases = max(Ylabels); % number of simulated cases

        x_glm = zeros(npts,n_of_cases);         y_glm = zeros(npts,n_of_cases);
        x_glm_dhrf = zeros(npts,n_of_cases);    y_glm_dhrf = zeros(npts,n_of_cases);
        x_glm_oracle = zeros(npts,n_of_cases);  y_glm_oracle = zeros(npts,n_of_cases);
        x_fglm_lr = zeros(npts,n_of_cases);     y_fglm_lr = zeros(npts,n_of_cases);
        x_fglm_decomp = zeros(npts,n_of_cases); y_fglm_decomp = zeros(npts,n_of_cases);
        x_glm_dict = zeros(npts,n_of_cases);    y_glm_dict = zeros(npts,n_of_cases);
        x_glm_wav = zeros(npts,n_of_cases);     y_glm_wav = zeros(npts,n_of_cases);

        auc_glm = zeros(n_of_cases,1);
        auc_glm_dhrf = zeros(n_of_cases,1);
        auc_glm_oracle = zeros(n_of_cases,1);
        auc_fglm_lr = zeros(n_of_cases,1);
        auc_fglm_decomp = zeros(n_of_cases,1);
        auc_glm_dict = zeros(n_of_cases,1);
        auc_glm_wav = zeros(n_of_cases,1);

        for c = 1:n_of_cases

            map = t_glm_oracle(:);
            tnoise = map(Ylabels == 0);
            t = map(Ylabels == c);
            gold = [zeros(length(tnoise),1);ones(length(t),1)];
            data = [tnoise;t];
            [x_glm_oracle(:,c),y_glm_oracle(:,c)] = mrROC(data,gold,npts);
            auc_glm_oracle(c) = abs(trapz(x_glm_oracle(:,c),y_glm_oracle(:,c)));

            map = t_glm(:);
            tnoise = map(Ylabels == 0);
            t = map(Ylabels == c);
            gold = [zeros(length(tnoise),1);ones(length(t),1)];
            data = [tnoise;t];
            [x_glm(:,c),y_glm(:,c)] = mrROC(data,gold,npts);
            auc_glm(c) = abs(trapz(x_glm(:,c),y_glm(:,c)));

            map = t_glm_dhrf(:);
            tnoise = map(Ylabels == 0);
            t = map(Ylabels == c);
            gold = [zeros(length(tnoise),1);ones(length(t),1)];
            data = [tnoise;t];
            [x_glm_dhrf(:,c),y_glm_dhrf(:,c)] = mrROC(data,gold,npts);
            auc_glm_dhrf(c) = abs(trapz(x_glm_dhrf(:,c),y_glm_dhrf(:,c)));

            map = t_fglm_lr(:);
            tnoise = map(Ylabels == 0);
            t = map(Ylabels == c);
            gold = [zeros(length(tnoise),1);ones(length(t),1)];
            data = [tnoise;t];
            [x_fglm_lr(:,c),y_fglm_lr(:,c)] = mrROC(data,gold,npts);
            auc_fglm_lr(c) = abs(trapz(x_fglm_lr(:,c),y_fglm_lr(:,c)));

            map = t_fglm_decomp(:);
            tnoise = map(Ylabels == 0);
            t = map(Ylabels == c);
            gold = [zeros(length(tnoise),1);ones(length(t),1)];
            data = [tnoise;t];
            [x_fglm_decomp(:,c),y_fglm_decomp(:,c)] = mrROC(data,gold,npts);
            auc_fglm_decomp(c) = abs(trapz(x_fglm_decomp(:,c),y_fglm_decomp(:,c)));

            if ~fastop
                map = t_glm_dict;
                labels = map(end,:);
                tnoise = map(c,labels == 0);
                t = map(c,labels == 1);
                gold = [zeros(length(tnoise),1);ones(length(t),1)];
                data = [tnoise';t'];
                [x_glm_dict(:,c),y_glm_dict(:,c)] = mrROC(data,gold,npts);
                auc_glm_dict(c) = abs(trapz(x_glm_dict(:,c),y_glm_dict(:,c)));

                map = t_glm_wav;
                labels = map(end,:);
                tnoise = map(c,labels == 0);
                t = map(c,labels == 1);
                gold = [zeros(length(tnoise),1);ones(length(t),1)];
                data = [tnoise';t'];
                [x_glm_wav(:,c),y_glm_wav(:,c)] = mrROC(data,gold,npts);
                auc_glm_wav(c) = abs(trapz(x_glm_wav(:,c),y_glm_wav(:,c)));
            end

        end

        name2save = ['results/aucs_varHRF_oversubjects_SNR',...
            strrep(num2str(snr), '.', 'p'),...
            '_dur',num2str(duration)];


        if fastop
            
            figure(1)
            plot(ttps,auc_glm_oracle,'b','linewidth',3)
            hold on
            plot(ttps,auc_glm,'k','linewidth',3)
            plot(ttps,auc_glm_dhrf,'y','linewidth',3)
            plot(ttps,auc_fglm_lr,'r','linewidth',3)
            plot(ttps,auc_fglm_decomp,'g','linewidth',3)
            
            legend({'GLM oracle','GLM','GLM + HRF derivatives',...
                'fuzzy GLM-LR','fuzzy GLM-\alpha'},...
                'Location','SouthEast')
            title(strrep(name2save, '_', ' '))
            xlabel('Time to peak (in seconds)','fontsize',24)
            ylabel('Area under the ROC curves','fontsize',24)
            grid on

            set(gca,'fontsize',22)
            hold off
        else
            
            figure(1)
            plot(ttps,auc_glm_oracle,'b','linewidth',3)
            hold on
            plot(ttps,auc_glm,'k','linewidth',3)
            plot(ttps,auc_glm_dhrf,'y','linewidth',3)
            plot(ttps,auc_fglm_lr,'r','linewidth',3)
            plot(ttps,auc_fglm_decomp,'g','linewidth',3)
            plot(ttps,auc_glm_dict,'c','linewidth',3)
            plot(ttps,auc_glm_wav,'m','linewidth',3)

            legend({'GLM oracle','GLM','GLM + HRF derivatives',...
                'fuzzy GLM-LR','fuzzy GLM-\alpha', 'Sparse GLM', 'GLM Wavelets'},...
                'Location','SouthEast')
            title(strrep(name2save, '_', ' '))
            xlabel('Time to peak (in seconds)','fontsize',24)
            ylabel('Area under the ROC curves','fontsize',24)
            grid on

            set(gca,'fontsize',22)
            hold off
        end
        save_fig(name2save)

        if ~fastop
            save([name2save,'.mat'],...
                'snr',...
                'duration',...
                'x_glm_oracle', 'y_glm_oracle',...
                'x_glm', 'y_glm',...
                'x_glm_dhrf', 'y_glm_dhrf',...
                'x_fglm_lr', 'y_fglm_lr',...
                'x_fglm_decomp', 'y_fglm_decomp',...
                'x_glm_dict', 'y_glm_dict',...
                'x_glm_wav', 'y_glm_wav',...
                'auc_glm_oracle',...
                'auc_glm',...
                'auc_glm_dhrf',...
                'auc_fglm_lr',...
                'auc_fglm_decomp',...
                'auc_glm_dict',...
                'auc_glm_wav',...
                'ttps',...
                'elapsed_time')
        end
    end
end

