function save_fig(name)

set(gca,'fontsize',15)
%print(gcf,'-dpsc2',[name,'.eps']);
print(gcf,'-dtiff',[name,'.tiff']);
%print(gcf,'-dpng',[name,'.png']);